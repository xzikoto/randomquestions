﻿using AutoMapper;
using RandomQuestions.Data.Entities;
using RandomQuestions.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Services.DTOMappers
{
    public class DTOAutomapper :  Profile
    {
        public DTOAutomapper()
        {
            CreateMap<Question, QuestionDTO>().ReverseMap();

            CreateMap<Category, CategoryDTO>().ReverseMap();

            CreateMap<Answer, AnswerDTO>().ReverseMap();

        }
    }
}
