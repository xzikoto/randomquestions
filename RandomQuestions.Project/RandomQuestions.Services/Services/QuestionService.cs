﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RandomQuestions.Data.Context;
using RandomQuestions.Services.Contracts;
using RandomQuestions.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Linq;
using System.Threading.Tasks;
using RandomQuestions.Data.Entities;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;

namespace RandomQuestions.Services.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IMapper mapper;
        private readonly RandomQuestionsContext dbcontext;
        private readonly IDateTimeProvider dateTimeProvider;
        //TO DO: ADD TOAST NOTIFICATIONs IN ORDER TO BE MORE USER FRIENDLY AND STYLISH

        public QuestionService(IMapper mapper,
                              RandomQuestionsContext dbcontext,
                              IDateTimeProvider dateTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
        }

        public AnswerDTO GetLastAnswer(int userId)
        {
            var lastAnswer = dbcontext.Answers.FirstOrDefault(answer => answer.UserId == userId);

            if (lastAnswer != null)
            {
                try
                {
                    dbcontext.SaveChanges();
                    return mapper.Map<AnswerDTO>(lastAnswer);
                }
                catch (InvalidOperationException e)
                {
                    throw new Exception(e.Message);
                }
            }
            else
            {
                return null;
            }

        }

        public Task<List<string>> CheckAnswers(string[] data, int userId)
        {
            List<string> answers = new List<string>();
            List<string> userScores = new List<string>();

            answers = data.ToList();

            var questions = dbcontext.Questions.Include(question => question.Category)
                                               .Where(q => q.IncorrectAnswers != null && q.CorrectAnswer != null);

            var incorrectAnswers = new List<string>();

            foreach (var item in questions)
            {
                incorrectAnswers.AddRange(item.IncorrectAnswers);
            }

            foreach (var item in answers)
            {
                var questionExist = questions.Where(q => q.CorrectAnswer.ToLower() == item.ToLower()).FirstOrDefault();

                if (questionExist != null)
                {
                    userScores.Add("TRUE " + "CATEGORY: " + questionExist.Category.Name + "Question: " + questionExist.Name);
                }
                else
                {
                    var isItIncorrect = incorrectAnswers.Any(value => value.ToLower() == item.ToLower());

                    if (isItIncorrect)
                    {
                        var allQuestion = questions.Where(question => question.IncorrectAnswers != null).ToList();
                        var questionEntity = allQuestion.FirstOrDefault(x => x.IncorrectAnswers.Contains(item));

                        userScores.Add("FALSE " + "CATEGORY: " + questionEntity.Category.Name + "Question: " + questionEntity.Name);
                    }
                }

            }

            try
            {
                AnswerDTO answer = new AnswerDTO();
                answer.Scores = userScores;
                answer.UserId = userId;

                var answerEntity = mapper.Map<Answer>(answer);

                dbcontext.Answers.Add(answerEntity);
                dbcontext.SaveChanges();
            }
            catch (InvalidOperationException e)
            {

                throw new Exception(e.Message);
            }

            return Task.FromResult(userScores);
        }

        public Task<bool> CheckSingleAnswer(string data)
        {
            return Task.FromResult(true);
        }

        public List<QuestionDTO> GetFiveRandomQuestions(Random rnd)
        {
           var list = FiveRandomQuestionsLogic(rnd).ToArray();

            //History currentData = new History()
            //{
            //    FirrstQuestionID = list[0].Id,
            //    SecondQuestionID = list[1].Id,
            //    ThirdQuestionID  = list[2].Id,
            //    FourthQuestionID = list[3].Id
            //};

            //dbcontext.History.Add(currentData);

            try
            {
                dbcontext.SaveChanges();
            }
            catch (ValidationException e)
            {
                throw new ValidationException(e.Message);
            }

            return list.ToList();
        }

        private List<QuestionDTO> CheckIfMatchWithPrevious(List<int> fiveRandomQuestionsIDs)
        {
            //var previous = dbcontext.History.ToList().Last();

            //var ItContains = fiveRandomQuestionsIDs.Any(question => question == previous.FirtQuestionID &&
            //                                                          question == previous.FirrstQuestionID &&
            //                                                          question == previous.FirrstQuestionID &&
            //                                                          question == previous.FirrstQuestionID);
            //int counter = 0;
            //if (ItContains)
            //{
            //    //Mixing Logic
            //}

            return new List<QuestionDTO>();
        }

        private List<QuestionDTO> FiveRandomQuestionsLogic(Random rnd)
        {

            rnd = new Random(DateTime.Now.Millisecond);
            var number = rnd.Next(1, 20);

            var questions = dbcontext.Questions.Include(question => question.Category);

            //Should optimize this
            var artQuestion = mapper.Map<QuestionDTO>(questions.Where(question => question.Category.Name == "Animals").Skip(number - 1).Take(1).FirstOrDefault());
            rnd = new Random(DateTime.Now.Millisecond);
            number = rnd.Next(1, 20);

            var sportQuestion = mapper.Map<QuestionDTO>(questions.Where(question => question.Category.Name == "Art").Skip(number - 1).Take(1).FirstOrDefault());
            rnd = new Random(DateTime.Now.Millisecond);
            number = rnd.Next(1, 20);

            var technologyQuestion = mapper.Map<QuestionDTO>(questions.Where(question => question.Category.Name == "Science: Computers").Skip(number - 1).Take(1).FirstOrDefault());
            rnd = new Random(DateTime.Now.Millisecond);
            number = rnd.Next(1, 20);

            var animalsQuestion = mapper.Map<QuestionDTO>(questions.Where(question => question.Category.Name == "Sports").Skip(number - 1).Take(1).FirstOrDefault());


            //Make a  mediator list to check every time if prevous questions are the same as the current one
            List<QuestionDTO> fiveRandomQuestions = new List<QuestionDTO>();
            List<int> fiveRandomQuestionsIDs = new List<int>();


            fiveRandomQuestions.Add(artQuestion);
            fiveRandomQuestionsIDs.Add(artQuestion.Id);

            fiveRandomQuestions.Add(animalsQuestion);
            fiveRandomQuestionsIDs.Add(animalsQuestion.Id);

            fiveRandomQuestions.Add(technologyQuestion);
            fiveRandomQuestionsIDs.Add(technologyQuestion.Id);

            fiveRandomQuestions.Add(sportQuestion);
            fiveRandomQuestionsIDs.Add(sportQuestion.Id);

            CheckIfMatchWithPrevious(fiveRandomQuestionsIDs);

            return fiveRandomQuestions;
        }
            public List<int> GetAllTimeScore(int userId)
        {
            //HARDCODED 
            userId = 1;

            var answersFromUser = dbcontext.Answers.Where(answer => answer.UserId == userId);

            List<int> allTimeScore = new List<int>();
            List<string> allInfos = new List<string>();

            int allRightAnswers = 0;
            int allWrongAnswers = 0;

            var scores = answersFromUser;

            foreach (var item in scores)
            {
                allInfos.AddRange(item.Scores);
            }

            foreach (var item in allInfos)
            {
                if (item.StartsWith("TRUE"))
                {
                    allRightAnswers++;
                }
                else
                {
                    allWrongAnswers++;
                }
            }

            allTimeScore.Add(allRightAnswers);
            allTimeScore.Add(allWrongAnswers);

            return allTimeScore;
        }

        public Task CheckSingleAnswers(string data)
        {
            throw new NotImplementedException();
        }
    }
}
