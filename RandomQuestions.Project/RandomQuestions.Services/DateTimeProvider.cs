﻿using RandomQuestions.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.UtcNow;
    }
}
