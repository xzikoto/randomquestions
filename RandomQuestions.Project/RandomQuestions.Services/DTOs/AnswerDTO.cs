﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Services.DTOs
{
    public class AnswerDTO
    {
        public int UserId { get; set; }
        public List<string> Scores { get; set; }
    }
}
