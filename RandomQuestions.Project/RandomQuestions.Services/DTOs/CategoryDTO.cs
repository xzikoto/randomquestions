﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Services.DTOs
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
