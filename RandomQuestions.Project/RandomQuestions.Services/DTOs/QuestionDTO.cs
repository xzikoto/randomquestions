﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Services.DTOs
{
    public class QuestionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int CategoryId { get; set; }
        public CategoryDTO Category { get; set; }

        public string CorrectAnswer { get; set; }
        public List<string> IncorrectAnswers { get; set; }
    }
}
