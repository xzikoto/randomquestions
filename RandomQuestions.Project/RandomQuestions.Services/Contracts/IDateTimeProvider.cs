﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Services.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
