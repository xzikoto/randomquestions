﻿using RandomQuestions.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RandomQuestions.Services.Contracts
{
    public interface IQuestionService
    {
        AnswerDTO GetLastAnswer(int userId);
        List<int> GetAllTimeScore(int userId);

        List<QuestionDTO> GetFiveRandomQuestions(Random rnd);
        Task<List<string>> CheckAnswers(string[] data,  int userId);
        Task CheckSingleAnswers(string data);

    }
}
