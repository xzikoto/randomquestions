﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RandomQuestions.Data.Context;
using RandomQuestions.Services.DTOs;
using RandomQuestions.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Service.Tests.Answer
{
    [TestClass]
    public class GetLastAnswer_Should : BaseTest
    {
        const int FAKE_ID = 1;
        [TestMethod]
        public void GetLastAnswer_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetLastAnswer_When_ParamsAreValid));

            List<string> scores = new List<string>();
            scores.Add("");

            var answerDTO = new AnswerDTO { UserId = FAKE_ID, Scores = scores };
            
            //Act & Assert
            using (var assertContext = new RandomQuestionsContext(options))
            {
                var sut = new QuestionService(mapper, assertContext, dateTimeProvider);

                var result = sut.GetLastAnswer(FAKE_ID);

                Assert.AreEqual(answerDTO.UserId, result.UserId);
                Assert.AreEqual(answerDTO.Scores, result.Scores);

              //If my Entities had  property indicating when are create on I would  test it like the code above :)
              //  Assert.AreEqual(dateTimeProvider.GetDateTime(), assertContext.Answers.Find(answerDTO.UserId).CreatedOn);
            }
        }
    }
}
