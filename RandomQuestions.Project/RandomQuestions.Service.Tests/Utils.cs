﻿using Microsoft.EntityFrameworkCore;
using RandomQuestions.Data.Context;

namespace RandomQuestions.Service.Tests
{
    public class Utils
    {
        public static DbContextOptions<RandomQuestionsContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<RandomQuestionsContext>().UseInMemoryDatabase(databaseName).Options;
        }
    }
}
