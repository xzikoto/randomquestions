﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RandomQuestions.Data.Context;
using RandomQuestions.Services.Contracts;
using RandomQuestions.Services.DTOMappers;
using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Service.Tests
{
    public abstract class BaseTest
    {
        protected IMapper mapper;
        protected IDateTimeProvider dateTimeProvider;

        public BaseTest()
        {
            var config = new MapperConfiguration(opts => opts.AddProfile(new DTOAutomapper()));
            mapper = config.CreateMapper();

            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDateTime())
                .Returns(Convert.ToDateTime("02-20-2020"));

            dateTimeProvider = mock.Object;
        }

        [TestCleanup()]
        public void Cleanup()
        {
            var options = Utils.GetOptions(nameof(TestContext.TestName));

            var context = new RandomQuestionsContext(options);
            context.Database.EnsureDeleted();
        }
    }
}
