﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RandomQuestions.Data.Entities;
using RandomQuestions.Data.Seeder;
using System;

namespace RandomQuestions.Data.Context
{
    public class RandomQuestionsContext  : IdentityDbContext<User,Role, int>
    {
        public RandomQuestionsContext(DbContextOptions<RandomQuestionsContext> options) 
            : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<History> History { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Seeder();

            builder.Entity<Question>()
           .Property(e => e.IncorrectAnswers)
           .HasConversion(
               v => string.Join(',', v),
               v => v.Split(',', StringSplitOptions.None));
            
            builder.Entity<Answer>()
           .Property(e => e.Scores)
           .HasConversion(
               v => string.Join(',', v),
               v => v.Split(',', StringSplitOptions.None));

            base.OnModelCreating(builder);
        }
    }
}
