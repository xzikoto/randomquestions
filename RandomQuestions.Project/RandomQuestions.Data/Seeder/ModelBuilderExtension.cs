﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using RandomQuestions.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RandomQuestions.Data.Seeder
{
    public static class ModelBuilderExtension
    {
        private const string SEED_DATA_FILEPATH = @"C:\Users\ivail\Desktop\randomquestions\RandomQuestions.Project\RandomQuestions.Data\Seeder\dataSeed.csv";
        public static void Seeder(this ModelBuilder builder)
        {
            var rawQuestions = ReadRawData();


            var categories = CreateCategoryModels(rawQuestions);
            builder.Entity<Category>().HasData(categories);

            var questions = CreateQuestionModels(rawQuestions, categories);
            builder.Entity<Question>().HasData(questions);

            //SEEDING ROLES
            builder.Entity<Role>().HasData(
                new Role { Id = 1, Name = "Manager", NormalizedName = "MANAGER" },
                new Role { Id = 2, Name = "Member", NormalizedName = "MEMBER" }
            );


            //SEEDING MANAGER ACCOUNT
            var hasher = new PasswordHasher<User>();

            User managerUser = new User
            {
                Id = 1,
                UserName = "manager@bo.com",
                NormalizedUserName = "MANAGER@BO.COM",
                Email = "manager@bo.com",
                NormalizedEmail = "MANAGER@BO.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = true,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN"
            };

            managerUser.PasswordHash = hasher.HashPassword(managerUser, "P@ssw0rd");

            builder.Entity<User>().HasData(managerUser);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = managerUser.Id
                });


            //SEEDING USERS
            var user1 = new User
            {
                Id = 2,
                UserName = "JonathanRandall@Gmail.Com",
                Email = "JonathanRandall@Gmail.Com",
                CreatedOn = DateTime.UtcNow,
                IsDeleted = false,
                isBanned = false
            };


            builder.Entity<User>().HasData(user1);
        }

        private static List<Dictionary<string, string>> ReadRawData()
        {
            var questions = new List<Dictionary<string, string>>();

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; 
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, SEED_DATA_FILEPATH);

            using (TextFieldParser parser = new TextFieldParser(path))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");

                while (!parser.EndOfData)
                {
                    var questionRawData = new Dictionary<string, string>();
                    string[] fields = parser.ReadFields();

                    questionRawData.Add("Category", fields[0]);
                    questionRawData.Add("Type of question", fields[1]);
                    questionRawData.Add("Difficulty", fields[2]);
                    questionRawData.Add("Question", fields[3]);
                    questionRawData.Add("CorrectAnswer", fields[4]);
                    questionRawData.Add("FirstIncorrectAnswer", fields[5]);
                    questionRawData.Add("SecondIncorrectAnswer", fields[6]);
                    questionRawData.Add("ThirdIncorrectAnswer", fields[7]);

                    questions.Add(questionRawData);
                }
            }

            return questions;
        }

        private static List<Category> CreateCategoryModels(List<Dictionary<string, string>> rawQuestions)
        {
            var countryNames = GetCategories(rawQuestions, "Category");
            var categories = new List<Category>();
            var counter = 1;

            foreach (var name in countryNames)
            {
                var country = new Category() { Id = counter, Name = name };
                counter++;
                categories.Add(country);
            }

            return categories;
        }

        private static List<Question> CreateQuestionModels(List<Dictionary<string, string>> rawQuestions, List<Category> categories)
        {
            var questions = new List<Question>();
            var counter = 1;

            foreach (var rawQuestion in rawQuestions)
            {
                var category = categories.Where(category => category.Name == rawQuestion["Category"]).First();

                var question = new Question()
                {
                    Id = counter,
                    Name = rawQuestion["Question"],
                    CorrectAnswer = rawQuestion["CorrectAnswer"],
                    Difficulty = rawQuestion["Difficulty"],
                    CategoryId = category.Id,
                    //TO DO: NOT VERY EFFECTIVE - MODIFY IT LATER
                    IncorrectAnswers = new string[] { rawQuestion["FirstIncorrectAnswer"], rawQuestion["SecondIncorrectAnswer"], rawQuestion["ThirdIncorrectAnswer"], }
                };

                questions.Add(question);
                counter++;
            }

            return questions;
        }

        private static HashSet<string> GetCategories(List<Dictionary<string, string>> rawBeers, string nameKey)
        {
            var names = new HashSet<string>();

            foreach (var rawBeer in rawBeers)
            {
                names.Add(rawBeer[nameKey]);
            }

            return names;
        }
    }
}
