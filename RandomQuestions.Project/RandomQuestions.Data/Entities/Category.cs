﻿using RandomQuestions.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Data.Entities
{
    public class Category : Entity
    {
        public int Id  { get; set; }
        public string Name { get; set; }
    }
}
