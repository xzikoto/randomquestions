﻿using RandomQuestions.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RandomQuestions.Data.Entities
{
    public class Question :Entity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public string CorrectAnswer { get; set; }
        public string Difficulty { get; set; }
        public string Type { get; set; }
        public string[] IncorrectAnswers { get; set; }
    }
}
