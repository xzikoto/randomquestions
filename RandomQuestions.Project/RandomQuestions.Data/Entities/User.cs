﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace RandomQuestions.Data.Entities
{
    public class User : IdentityUser<int>
    {
        [Key]
        public override int Id { get; set; }

        //Every Entity's duty to the database
        public bool isBanned { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
