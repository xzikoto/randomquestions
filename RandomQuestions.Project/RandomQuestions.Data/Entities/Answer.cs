﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Data.Entities
{
    public class Answer
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public string[] Scores { get; set; }
    }
}
