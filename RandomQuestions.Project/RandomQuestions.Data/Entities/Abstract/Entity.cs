﻿using RandomQuestions.Data.Entities.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Data.Entities.Abstract
{
    public abstract class Entity : IAuditable, IDeletable
    {
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
