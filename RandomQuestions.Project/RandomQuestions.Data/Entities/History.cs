﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomQuestions.Data.Entities
{
    public class History
    {
        public int Id { get; set; }

        public int FirrstQuestionID { get; set; }
        public int SecondQuestionID { get; set; }
        public int ThirdQuestionID { get; set; }
        public int FourthQuestionID { get; set; }
    }
}
