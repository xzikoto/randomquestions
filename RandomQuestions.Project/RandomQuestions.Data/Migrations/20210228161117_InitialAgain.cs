﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RandomQuestions.Data.Migrations
{
    public partial class InitialAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "e62fe86e-8955-48c8-8c98-3b3dcdff5ca9");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "bb13d6a9-da70-4920-913c-2984a0d858e2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "38c8896a-106e-4d95-bac3-ed21a8ff42c1", new DateTime(2021, 2, 28, 16, 11, 17, 297, DateTimeKind.Utc).AddTicks(594), "AQAAAAEAACcQAAAAECpFThcVlfAYH1B9ph2Jl04KHFkbdkztNewCh0J33sfMD6Qspl3apgatoa4AIlHfRQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn" },
                values: new object[] { "ee46486c-bf48-4092-a170-b3a53f091f54", new DateTime(2021, 2, 28, 16, 11, 17, 308, DateTimeKind.Utc).AddTicks(2170) });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 292, DateTimeKind.Utc).AddTicks(9135));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 293, DateTimeKind.Utc).AddTicks(9));

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 3, new DateTime(2021, 2, 28, 16, 11, 17, 293, DateTimeKind.Utc).AddTicks(28), null, false, null, "Sports" },
                    { 4, new DateTime(2021, 2, 28, 16, 11, 17, 293, DateTimeKind.Utc).AddTicks(71), null, false, null, "History" },
                    { 6, new DateTime(2021, 2, 28, 16, 11, 17, 293, DateTimeKind.Utc).AddTicks(76), null, false, null, "Art" },
                    { 5, new DateTime(2021, 2, 28, 16, 11, 17, 293, DateTimeKind.Utc).AddTicks(72), null, false, null, "Science: Computers" }
                });

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 294, DateTimeKind.Utc).AddTicks(9748));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2656));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2729));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2740));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2749));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2760));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2768));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2777));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2788));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2797));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2804));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 12,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2813));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 13,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2823));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 14,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2829));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 15,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2837));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 16,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2845));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 17,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2852));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 18,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2861));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 19,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2869));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 20,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2933));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 21,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2943));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 22,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2952));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 23,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2960));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 24,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2967));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 25,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2975));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 26,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2983));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 27,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2992));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 28,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(2999));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 29,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3008));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 30,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3016));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 31,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3023));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 32,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3030));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 33,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3039));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 34,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3049));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 35,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3056));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 36,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3064));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 37,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3073));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 38,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3082));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 39,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3090));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 40,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3101));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 41,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3109));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 42,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3117));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 43,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3126));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 44,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3183));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 45,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3192));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 46,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3201));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 47,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3209));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 48,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3218));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 49,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3226));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 50,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3234));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 51,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3243));

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CategoryId", "CorrectAnswer", "CreatedOn", "DeletedOn", "Difficulty", "IncorrectAnswers", "IsDeleted", "ModifiedOn", "Name", "Type" },
                values: new object[,]
                {
                    { 52, 3, "Horse-Riding", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3252), null, "easy", "Cycling,Swimming,Running", false, null, "Which of the following sports is not part of the triathlon?", null },
                    { 160, 5, "Inkscape", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4413), null, "medium", "Paint.NET,GIMP,Adobe Photoshop", false, null, "All of the following programs are classified as raster graphics editors EXCEPT:", null },
                    { 161, 5, "HD Graphics 500", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4422), null, "easy", "HD Graphics 700,HD Graphics 600,HD Graphics 7000", false, null, "The series of the Intel HD graphics generation succeeding that of the 5000 and 6000 series (Broadwell) is called:", null },
                    { 162, 5, "Taiwan", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4431), null, "medium", "United States,Germany,China (People&#039;s Republic of)", false, null, "The computer OEM manufacturer Clevo, known for its Sager notebook line, is based in which country?", null },
                    { 163, 5, "Santa Clara", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4440), null, "medium", "Palo Alto,Cupertino,Mountain View", false, null, "Nvidia&#039;s headquarters are based in which Silicon Valley city?", null },
                    { 164, 5, ".tv", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4448), null, "easy", ".tu,.tt,.tl", false, null, "What is the domain name for the country Tuvalu?", null },
                    { 165, 5, "1000000", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4456), null, "easy", "1024,1000,1048576", false, null, "How many kilobytes in one gigabyte (in decimal)?", null },
                    { 166, 5, "Mystic Mansion", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4464), null, "medium", "Trusty Tahr,Utopic Unicorn,Wily Werewolf", false, null, "Which one of these is not an official development name for a Ubuntu release?", null },
                    { 167, 5, "&lt;marquee&gt;&lt;/marquee&gt;", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4473), null, "medium", "&lt;scroll&gt;&lt;/scroll&gt;,&lt;move&gt;&lt;/move&gt;,&lt;slide&gt;&lt;/slide&gt;", false, null, "In HTML, which non-standard tag used to be be used to make elements scroll across the viewport?", null },
                    { 168, 5, "center", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4482), null, "medium", "static,absolute,relative", false, null, "In CSS, which of these values CANNOT be used with the &quot;position&quot; property?", null },
                    { 169, 5, "Heartbleed", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4489), null, "hard", "Shellshock,Corrupted Blood,Shellscript", false, null, "Which of these was the name of a bug found in April 2014 in the publicly available OpenSSL cryptography library?", null },
                    { 170, 5, "Green", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4497), null, "easy", "Red,Blue,Yellow", false, null, "In &quot;Hexadecimal&quot;, what color would be displayed from the color code? &quot;#00FF00&quot;?", null },
                    { 171, 5, "Python", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4505), null, "easy", "C#,C++,Java", false, null, "Which computer language would you associate Django framework with?", null },
                    { 172, 5, "Assembly", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4513), null, "medium", "Python,C#,Pascal", false, null, "Which of these programming languages is a low-level language?", null },
                    { 173, 5, "Long Term Support", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4521), null, "easy", "Long Taco Service,Ludicrous Transfer Speed,Ludicrous Turbo Speed", false, null, "What does LTS stand for in the software market?", null },
                    { 174, 5, "10 Gb/s", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4529), null, "medium", "5 Gb/s,8 Gb/s,1 Gb/s", false, null, "How fast is USB 3.1 Gen 2 theoretically?", null },
                    { 175, 5, "Mac OS", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4538), null, "medium", "Windows,Linux,OS/2", false, null, "Which operating system was released first?", null },
                    { 176, 5, "Serbia", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4548), null, "medium", "Romania,Russia,Rwanda", false, null, ".rs is the top-level domain for what country?", null },
                    { 159, 5, "Captures what&#039;s on the screen and copies it to your clipboard", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4405), null, "easy", "Nothing,Saves a .png file of what&#039;s on the screen in your screenshots folder in photos,Closes all windows", false, null, "What does the Prt Sc button do?", null },
                    { 177, 5, "Hexidecimal", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4557), null, "easy", "Binary,Duodecimal,Octal", false, null, "The numbering system with a radix of 16 is more commonly referred to as", null },
                    { 158, 5, "C#", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4394), null, "medium", "Java,C++,Objective-C", false, null, "Which of the following languages is used as a scripting language in the Unity 3D game engine?", null },
                    { 156, 5, "Active Directory", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4330), null, "medium", "Alternative Drive,Automated Database,Active Department", false, null, "What does AD stand for in relation to Windows Operating Systems?", null },
                    { 139, 4, "October 1, 1949", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4190), null, "medium", "April 3, 1947,May 7, 1945,December 6, 1950", false, null, "When was the People&#039;s Republic of China founded?", null },
                    { 140, 4, "General Electric", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4200), null, "medium", "Colt Firearms,Heckler &amp; Koch,Sig Sauer", false, null, "The minigun was designed in 1960 by which manufacturer.", null },
                    { 141, 4, "USS Hornet", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4208), null, "medium", "USS Enterprise,USS Lexington,USS Saratoga", false, null, "On which aircraft carrier did the Doolitte Raid launch from on April 18, 1942 during World War II?", null },
                    { 142, 4, "Georgia", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4216), null, "easy", "Russia,Germany,Poland", false, null, "Which country was Josef Stalin born in?", null },
                    { 143, 4, "1869", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4224), null, "medium", "1859,1860,1850", false, null, "When did construction of the Suez Canal finish?", null },
                    { 144, 4, "Portugal", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4233), null, "easy", "Spain,The Netherlands,France", false, null, "Which of the following was Brazil was a former colony under?", null },
                    { 145, 4, "Franklin Roosevelt", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4240), null, "medium", "Theodore Roosevelt,George Washington,Abraham Lincoln", false, null, "Who was the only US President to be elected four times?", null },
                    { 146, 4, "D-Day", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4249), null, "medium", "Atomic bombings of Hiroshima and Nagasaki,Attack on Pearl Harbor,The Liberation of Paris", false, null, "What happened on June 6, 1944?", null },
                    { 147, 4, "235 AD", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4257), null, "medium", "235 BC,242 AD,210 AD", false, null, "When did the Crisis of the Third Century begin?", null },
                    { 148, 4, "Threw them out of a window", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4265), null, "hard", "Insulted their mothers,Locked them in stockades,Hung them.", false, null, "The Bohemian Revolt (1618-1620) started after Protestants in Prague did what to their Catholic Lords Regents?", null },
                    { 149, 4, "Jimmy Carter", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4273), null, "medium", "Ronald Reagan,Lydon B. Johnson,Gerald Ford", false, null, "Which U.S. President was famously &#039;attacked&#039; by a swimming rabbit?", null },
                    { 150, 4, "Gavrilo Princip", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4280), null, "medium", "Nedeljko Čabrinović,Oskar Potiorek,Ferdinand Cohen-Blind", false, null, "Who assassinated Archduke Franz Ferdinand?", null },
                    { 151, 4, "Australia", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4288), null, "hard", "India,Canada,Brazil", false, null, "Which country did the Eureka Rebellion, an 1856 battle against colonial rule, take place in?", null },
                    { 152, 5, "Instruction", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4296), null, "hard", "Address,Data,Control", false, null, "The Harvard architecture for micro-controllers added which additional bus?", null },
                    { 153, 5, "Central Processing Unit", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4305), null, "easy", "Central Process Unit,Computer Personal Unit,Central Processor Unit", false, null, "What does CPU stand for?", null },
                    { 154, 5, "Moving Picture", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4313), null, "easy", "Music Player,Multi Pass,Micro Point", false, null, "What does the &quot;MP&quot; stand for in MP3?", null },
                    { 155, 5, "Shellshock", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4321), null, "hard", "Heartbleed,Bashbug,Stagefright", false, null, "What was the name of the security vulnerability found in Bash in 2014?", null },
                    { 157, 5, "8", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4338), null, "easy", "1,2,64", false, null, "What amount of bits commonly equals one byte?", null },
                    { 178, 5, "Central Processing Unit", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4565), null, "medium", "Motherboard,Graphics Processing Unit,Keyboard", false, null, "What is known as &quot;the brain&quot; of the Computer?", null },
                    { 179, 5, "Stack", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4574), null, "hard", "Queue,Heap,Tree", false, null, "Which data structure does FILO apply to?", null }
                });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CategoryId", "CorrectAnswer", "CreatedOn", "DeletedOn", "Difficulty", "IncorrectAnswers", "IsDeleted", "ModifiedOn", "Name", "Type" },
                values: new object[,]
                {
                    { 180, 5, "Comprehensive documentation", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4583), null, "hard", "Individuals and interactions,Customer collaboration,Responding to change", false, null, "Which of these is not a key value of Agile software development?", null },
                    { 203, 6, "Neoplasticism", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4835), null, "hard", "Precisionism,Cubism,Impressionism", false, null, "Painter Piet Mondrian (1872 - 1944) was a part of what movement?", null },
                    { 204, 6, "2/2", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4843), null, "medium", "4/4,6/8,3/4", false, null, "Which time signature is commonly known as &ldquo;Cut Time?&rdquo;", null },
                    { 205, 6, "Michelangelo", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4852), null, "easy", "Leonardo da Vinci,Pablo Picasso,Raphael", false, null, "Who painted the Sistine Chapel?", null },
                    { 206, 6, "Fr&eacute;d&eacute;ric Auguste Bartholdi", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4861), null, "hard", "Jean-L&eacute;on G&eacute;r&ocirc;me,Auguste Rodin,Henri Matisse", false, null, "What French sculptor designed the Statue of Liberty?", null },
                    { 207, 6, "Andy Warhol", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4869), null, "medium", "Roy Lichtenstein,David Hockney,Peter Blake", false, null, "Which artist&rsquo;s studio was known as &#039;The Factory&#039;?", null },
                    { 208, 6, "N&uuml;rnberg", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4925), null, "hard", "Augsburg,Bamberg,Berlin", false, null, "Albrecht D&uuml;rer&#039;s birthplace and place of death were in...", null },
                    { 209, 6, "1504", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4933), null, "hard", "1487,1523,1511", false, null, "What year was the Mona Lisa finished?", null },
                    { 210, 6, "Salvador Dali", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4941), null, "easy", "Jackson Pollock,Vincent van Gogh,Edgar Degas", false, null, "Who painted &quot;Swans Reflecting Elephants&quot;, &quot;Sleep&quot;, and &quot;The Persistence of Memory&quot;?", null },
                    { 211, 6, "The Black Sea", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4950), null, "medium", "The Sea of Ice,Wanderer above the Sea of Fog,The Monk by the Sea", false, null, "Which one of these paintings is not by Caspar David Friedrich?", null },
                    { 212, 6, "Copic", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4958), null, "medium", "Dopix,Cofix,Marx", false, null, "Which of these are the name of a famous marker brand?", null },
                    { 213, 6, "Georges Seurat", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4966), null, "medium", "Paul C&eacute;zanne,Vincent Van Gogh,Henri Rousseau", false, null, "Which artist&#039;s style was to use small different colored dots to create a picture?", null },
                    { 214, 6, "Kobicha", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4975), null, "hard", "Byzantium,Pomp and Power,Palatinate", false, null, "Which of these is not an additional variation of the color purple?", null },
                    { 215, 6, "4", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4983), null, "hard", "1,3,2", false, null, "How many paint and pastel versions of &quot;The Scream&quot; is Norwegian painter Edvard Munch believed to have produced?", null },
                    { 216, 6, "Salvador Dali", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4992), null, "medium", "Pablo Picasso,Andy Warhol,Vincent van Gogh", false, null, "Who designed the Chupa Chups logo?", null },
                    { 217, 6, "The Starry Night", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(5000), null, "easy", "Wheatfields with Crows,The Sower with Setting Sun,The Church at Auvers", false, null, "Which Van Gogh painting depicts the view from his asylum in Saint-R&eacute;my-de-Provence in southern France?", null },
                    { 218, 6, "The Ninth Wave", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(5008), null, "easy", "Caf&eacute; Terrace at Night,Bedroom In Arles,Starry Night", false, null, "Which painting was not made by Vincent Van Gogh?", null },
                    { 219, 6, "Post-Impressionism", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(5016), null, "hard", "Romanticism,Neoclassical,Impressionism", false, null, "The painting &quot;The Starry Night&quot; by Vincent van Gogh was part of which art movement?", null },
                    { 202, 6, "Leonardo da Vinci", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4826), null, "easy", "Pablo Picasso,Claude Monet,Vincent van Gogh", false, null, "Who painted the Mona Lisa?", null },
                    { 201, 5, "Java Virtual Machine", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4813), null, "easy", "Java Vendor Machine,Java Visual Machine,Just Virtual Machine", false, null, "What does the computer software acronym JVM stand for?", null },
                    { 200, 5, "Peter Thiel", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4805), null, "hard", "Mark Zuckerberg,Marc Benioff,Jack Dorsey", false, null, "Who is the founder of Palantir?", null },
                    { 199, 5, "Secret sharing scheme", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4797), null, "hard", "Hashing algorithm,Asymmetric encryption,Stream cipher", false, null, "Which kind of algorithm is Ron Rivest not famous for creating?", null },
                    { 181, 5, "Motorola 68000", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4591), null, "medium", "Zilog Z80,Yamaha YM2612,Intel 8088", false, null, "What is the main CPU is the Sega Mega Drive / Sega Genesis?", null },
                    { 182, 5, "Apple", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4598), null, "hard", "IBM,Microsoft,Google", false, null, "What was the first company to use the term &quot;Golden Master&quot;?", null },
                    { 183, 5, "Cheetah", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4664), null, "hard", "Puma,Tiger,Leopard", false, null, "Released in 2001, the first edition of Apple&#039;s Mac OS X operating system (version 10.0) was given what animal code name?", null },
                    { 184, 5, "Java", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4674), null, "medium", "Python,Solaris OS,C++", false, null, "Which programming language was developed by Sun Microsystems in 1995?", null },
                    { 185, 5, "Transport", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4682), null, "hard", "Session,Data link,Network", false, null, "What is the name given to layer 4 of the Open Systems Interconnection (ISO) model?", null },
                    { 186, 5, "Injection", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4691), null, "hard", "Broken Authentication,Cross-Site Scripting,Insecure Direct Object References", false, null, "What vulnerability ranked #1 on the OWASP Top 10 in 2013?", null },
                    { 187, 5, "Radia Perlman", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4699), null, "hard", "Paul Vixie,Vint Cerf,Michael Roberts", false, null, "Who invented the &quot;Spanning Tree Protocol&quot;?", null },
                    { 188, 5, "200", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4707), null, "medium", "100,500,1000", false, null, "Approximately how many Apple I personal computers were created?", null },
                    { 138, 4, "Josip Broz Tito", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4182), null, "medium", "Karadjordje Petrovic,Milos Obilic,Aleskandar Petrovic", false, null, "Who was the leader of the Communist Party of Yugoslavia ?", null },
                    { 189, 5, "Java", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4715), null, "easy", "Python,C,Jakarta", false, null, "Which programming language shares its name with an island in Indonesia?", null },
                    { 191, 5, "Local Area Network", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4730), null, "easy", "Long Antenna Node,Light Access Node,Land Address Navigation", false, null, "In computing, what does LAN stand for?", null },
                    { 192, 5, "1024", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4739), null, "medium", "2400,1000,1240", false, null, "How many bytes are in a single Kibibyte?", null },
                    { 193, 5, "A&#039; + B&#039;", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4747), null, "hard", "A&#039;B + B&#039;A,A&#039;B&#039;,AB&#039; + AB", false, null, "According to DeMorgan&#039;s Theorem, the Boolean expression (AB)&#039; is equivalent to:", null },
                    { 194, 5, "C++", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4756), null, "hard", "Assembly,C#,ECMAScript", false, null, "What major programming language does Unreal Engine 4 use?", null },
                    { 195, 5, "Routing Information Protocol", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4764), null, "hard", "Runtime Instance Processes,Regular Interval Processes,Routine Inspection Protocol", false, null, "The acronym &quot;RIP&quot; stands for which of these?", null },
                    { 196, 5, "Shamir", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4772), null, "medium", "Secure,Schottky,Stable", false, null, "What does the &#039;S&#039; in the RSA encryption algorithm stand for?", null },
                    { 197, 5, "ALU", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4780), null, "hard", "CPU,RAM,Register", false, null, "Which of the following computer components can be built using only NAND gates?", null },
                    { 198, 5, "Cherry MX Blue", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4788), null, "hard", "Cherry MX Black,Cherry MX Red,Cherry MX Brown", false, null, "Which of these Cherry MX mechanical keyboard switches is both tactile and clicky?", null },
                    { 190, 5, "128 bits", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4723), null, "easy", "32 bits,64 bits,128 bytes", false, null, "How long is an IPv6 address?", null },
                    { 137, 4, "Operation Dragoon", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4174), null, "hard", "Operation Overlord,Operation Market Garden,Operation Torch", false, null, "What was the code name for the Allied invasion of Southern France on August 15th, 1944?", null }
                });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CategoryId", "CorrectAnswer", "CreatedOn", "DeletedOn", "Difficulty", "IncorrectAnswers", "IsDeleted", "ModifiedOn", "Name", "Type" },
                values: new object[,]
                {
                    { 136, 4, "Homer", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4166), null, "medium", "Aristotle,Odysseus,Socrates", false, null, "Who is attributed credit for recording the epic poem The Odyssey?", null },
                    { 135, 4, "753 BCE", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4159), null, "hard", "902 BCE,524 BCE,697 BCE", false, null, "When was the city of Rome, Italy founded?", null },
                    { 75, 3, "Nico Rosberg", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3496), null, "easy", "Lewis Hamilton,Max Verstappen,Kimi Raikkonen", false, null, "Who won the 2016 Formula 1 World Driver&#039;s Championship?", null },
                    { 76, 3, "Mercedes-AMG Petronas", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3505), null, "medium", "Scuderia Ferrari,McLaren Honda,Red Bull Racing Renault", false, null, "In 2016, who won the Formula 1 World Constructor&#039;s Championship for the third time in a row?", null },
                    { 77, 3, "Seattle Sounders", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3513), null, "easy", "Colorado Rapids,Toronto FC,Montreal Impact", false, null, "What team won the 2016 MLS Cup?", null },
                    { 78, 3, "Jules Bianchi", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3520), null, "medium", "Ayrton Senna,Ronald Ratzenberger,Gilles Villeneuve", false, null, "In Formula 1, the Virtual Safety Car was introduced following the fatal crash of which driver?", null },
                    { 79, 3, "2010", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3529), null, "easy", "2008,2009,2011", false, null, "What year did the New Orleans Saints win the Super Bowl?", null },
                    { 80, 3, "Madeira", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3537), null, "medium", "Terceira,Santa Maria,Porto Santo", false, null, "Which portuguese island is soccer player Cristiano Ronaldo from?", null },
                    { 81, 3, "Manchester United", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3546), null, "medium", "Barcelona,Bayern Munich,Liverpool", false, null, "Who won the &quot;Champions League&quot; in 1999?", null },
                    { 82, 3, "Ohio State Buckeyes", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3553), null, "medium", "Alabama Crimson Tide,Clemson Tigers,Wisconsin Badgers", false, null, "Who won the 2015 College Football Playoff (CFP) National Championship?", null },
                    { 83, 3, "1961", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3560), null, "medium", "1965,1959,1963", false, null, "What year was hockey legend Wayne Gretzky born?", null },
                    { 84, 3, "1969", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3571), null, "hard", "1968,1971,1970", false, null, "Which year was the third Super Bowl held?", null },
                    { 85, 3, "Jose Fernandez", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3579), null, "medium", "Jacob deGrom,Shelby Miller,Matt Harvey", false, null, "Which of the following pitchers was named National League Rookie of the Year for the 2013 season?", null },
                    { 86, 3, "Shaquille O&#039;Neal", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3588), null, "medium", "Allen Iverson,Kobe Bryant,Paul Pierce", false, null, "Which NBA player won Most Valuable Player for the 1999-2000 season?", null },
                    { 87, 3, "Skiing", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3596), null, "medium", "Swimming,Showjumping,Gymnastics", false, null, "In what sport does Fanny Chmelar compete for Germany?", null },
                    { 88, 3, "West Germany", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3604), null, "easy", "Soviet Union,Portugal,Brazil", false, null, "What team did England beat to win in the 1966 World Cup final?", null },
                    { 89, 3, "Portugal", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3613), null, "hard", "West Germany,Soviet Union,Brazil", false, null, "What team did England beat in the semi-final stage to win in the 1966 World Cup final?", null },
                    { 90, 3, "Roger Federer", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3673), null, "easy", "Bill Tilden,Boris Becker,Pete Sampras", false, null, "Who is often called &quot;the Maestro&quot; in the men&#039;s tennis circuit?", null },
                    { 91, 3, "13", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3684), null, "medium", "11,20,22", false, null, "How many premier league trophies did Sir Alex Ferguson win during his time at Manchester United?", null },
                    { 74, 3, "25", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3488), null, "easy", "19,69,41", false, null, "How many points did LeBron James score in his first NBA game?", null },
                    { 73, 3, "Germany", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3479), null, "easy", "Argentina,Brazil,Netherlands", false, null, "Which team won 2014 FIFA World Cup in Brazil?", null },
                    { 72, 3, "Pelle", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3470), null, "hard", "Insigne,Barzagli,Giaccherini", false, null, "Which Italian footballer told Neuer where he&#039;s putting his shot and dragging it wide, during the match Italy-Germany, UEFA EURO 2016?", null },
                    { 71, 3, "Mazda", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3462), null, "hard", "Toyota,Subaru,Nissan", false, null, "Which car company is the only Japanese company which won the 24 Hours of Le Mans?", null },
                    { 53, 3, "Jordan", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3261), null, "medium", "Benetton,Ferrari,Mercedes", false, null, "With which team did Michael Schumacher make his Formula One debut at the 1991 Belgian Grand Prix?", null },
                    { 54, 3, "Puma", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3270), null, "medium", "Nike,Adidas,Reebok", false, null, "Which German sportswear company&#039;s logo is the &#039;Formstripe&#039;?", null },
                    { 55, 3, "Duck", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3278), null, "medium", "Bye,Beamer,Carry", false, null, "What cricketing term denotes a batsman being dismissed with a score of zero?", null },
                    { 56, 3, "Big Daddy", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3286), null, "medium", "Giant Haystacks,Kendo Nagasaki,Masambula", false, null, "Who was the British professional wrestler Shirley Crabtree better known as?", null },
                    { 57, 3, "Saints", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3293), null, "medium", "Harlequins,Saracens,Wasps", false, null, "What is the nickname of Northampton town&#039;s rugby union club?", null },
                    { 58, 3, "Leicester City", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3302), null, "easy", "Northampton Town,Bradford City,West Bromwich Albion", false, null, "Which English football club has the nickname &#039;The Foxes&#039;?", null },
                    { 59, 3, "22", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3309), null, "easy", "20,24,26", false, null, "How many soccer players should be on the field at the same time?", null },
                    { 60, 3, "Badminton", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3318), null, "easy", "Table Tennis,Rugby,Cricket", false, null, "In what sport is a &quot;shuttlecock&quot; used?", null },
                    { 92, 3, "Leicester City", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3692), null, "easy", "Tottenham Hotspur,Watford,Stoke City", false, null, "Who won the premier league title in the 2015-2016 season following a fairy tale run?", null },
                    { 61, 3, "Golf Putting Green", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3326), null, "medium", "Football Pitch,Cricket Outfield,Pinball Table", false, null, "A stimpmeter measures the speed of a ball over what surface?", null },
                    { 63, 3, "Green", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3345), null, "medium", "Yellow,Brown,Blue", false, null, "In a game of snooker, what colour ball is worth 3 points?", null },
                    { 64, 3, "Death of Ayrton Senna (San Marino)", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3354), null, "medium", "The Showdown (Australia),Verstappen on Fire (Germany),Schumacher&#039;s Ban (Britain)", false, null, "The F1 season of 1994 is remembered for what tragic event?", null },
                    { 65, 3, "Lewis Hamilton", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3363), null, "easy", "Nico Rosberg,Sebastian Vettel,Jenson Button", false, null, "Who won the 2015 Formula 1 World Championship?", null },
                    { 66, 3, "Michael Schumacher", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3420), null, "easy", "Ayrton Senna,Fernando Alonso,Jim Clark", false, null, "Which driver has been the Formula 1 world champion for a record 7 times?", null },
                    { 67, 3, "7 - 1", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3428), null, "easy", "0 - 1,3 - 4,16 - 0", false, null, "What was the final score of the Germany vs. Brazil 2014 FIFA World Cup match?", null },
                    { 68, 3, "Moscow", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3436), null, "hard", "Barcelona,Tokyo,Los Angeles", false, null, "Where was the Games of the XXII Olympiad held?", null },
                    { 69, 3, "Penrith Panthers", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3445), null, "medium", "Melbourne Storm,Sydney Roosters,North Queensland Cowboys", false, null, "Josh Mansour is part of what NRL team?", null },
                    { 70, 3, "Porsche", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3453), null, "medium", "Toyota,Audi,Ferrari", false, null, "Which car manufacturer won the 2016 24 Hours of Le Mans?", null },
                    { 62, 3, "82", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3336), null, "medium", "62,42,102", false, null, "How many scoring zones are there on a conventional dart board?", null }
                });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CategoryId", "CorrectAnswer", "CreatedOn", "DeletedOn", "Difficulty", "IncorrectAnswers", "IsDeleted", "ModifiedOn", "Name", "Type" },
                values: new object[,]
                {
                    { 220, 6, "Pablo Picasso", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(5025), null, "medium", "Francisco Goya,Leonardo da Vinci,Henri Matisse", false, null, "Who painted the epic mural Guernica?", null },
                    { 93, 3, "Liverpool", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3701), null, "easy", "Real Madrid,Chelsea,Man City", false, null, "Who did Steven Gerrard win the Champions League with?", null },
                    { 95, 3, "Lewis Hamilton", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3716), null, "easy", "Sebastian Vettel,Nico Rosberg,Max Verstappen", false, null, "Who won the 2017 Formula One World Drivers&#039; Championship?", null },
                    { 118, 4, "August 19", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3983), null, "medium", "August 21,December 26,December 24", false, null, "On which day did the attempted coup d&#039;etat of 1991 in the Soviet Union begin?", null },
                    { 119, 4, "55", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3992), null, "medium", "50,60,54", false, null, "How old was Lyndon B. Johnson when he assumed the role of President of the United States?", null },
                    { 120, 4, "Afghanistan", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4000), null, "medium", "Turkmenistan,Kazakhstan,Uzbekistan", false, null, "Which of these countries was NOT a part of the Soviet Union?", null },
                    { 121, 4, "USS Pueblo", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4010), null, "medium", "USS North Carolina,USS Constitution,USS Indianapolis", false, null, "What is the name of the US Navy spy ship which was attacked and captured by North Korean forces in 1968?", null },
                    { 122, 4, "July 1st, 1867", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4018), null, "hard", "July 1st, 1763,July 1st, 1832,July 1st, 1902", false, null, "When did Canada leave the confederation to become their own nation?", null },
                    { 123, 4, "Richard Neville", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4026), null, "hard", "Richard III,Henry V,Thomas Warwick", false, null, "During the Wars of the Roses (1455 - 1487) which Englishman was dubbed &quot;the Kingmaker&quot;?", null },
                    { 124, 4, "Leader of the Ukrainian Cossacks", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4033), null, "hard", "General Secretary of the Communist Party of the USSR,Prince of Wallachia,Grand Prince of Novgorod", false, null, "Bohdan Khmelnytsky was which of the following?", null },
                    { 125, 4, "Robbing Trains", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4043), null, "medium", "Murder for Hire,Tax Evasion,Identity Fraud", false, null, "Joseph Stalin had a criminal past doing what?", null },
                    { 126, 4, "2004", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4051), null, "medium", "2006,2008,2002", false, null, "What year did the Boxing Day earthquake &amp; tsunami occur in the Indian Ocean?", null },
                    { 127, 4, "Kurdish", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4058), null, "medium", "Arab,Egyptian,Syrian", false, null, "What nationality was sultan Saladin?", null },
                    { 128, 4, "1776", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4067), null, "hard", "1775,1774,1777", false, null, "What year was the United States Declaration of Independence signed?", null },
                    { 129, 4, "March 11th, 1990", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4075), null, "hard", "December 25th, 1991,December 5th, 1991,April 20th, 1989", false, null, "When did Lithuania declare independence from the Soviet Union?", null },
                    { 130, 4, "The Columbian Exchange", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4086), null, "medium", "Triangle Trade,Transatlantic Slave Trade,The Silk Road", false, null, "What was the transfer of disease, crops, and people across the Atlantic shortly after the discovery of the Americas called?", null },
                    { 131, 4, "Dead Sea Scrolls", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4093), null, "hard", "The Blackbeard Chest,Sheep,The First Oaxaca Cave Sleeper", false, null, "What was found in 1946 by two young shepards near a cave?", null },
                    { 132, 4, "1997", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4101), null, "medium", "1986,1981,1971", false, null, "What year is considered to be the year that the British Empire ended?", null },
                    { 133, 4, "Poison", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4140), null, "easy", "Decapitation,Firing squad,Crucifixion", false, null, "How was Socrates executed?", null },
                    { 134, 4, "World War II", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(4149), null, "medium", "Taiping Rebellion,Three Kingdoms War,Mongol conquests", false, null, "Which historical conflict killed the most people?", null },
                    { 117, 4, "November 4, 1952", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3975), null, "medium", "July 26, 1908,July 1, 1973,November 25, 2002", false, null, "When was the United States National Security Agency established?", null },
                    { 116, 4, "X", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3966), null, "easy", "W,U,J", false, null, "The original Roman alphabet lacked the following letters EXCEPT:", null },
                    { 115, 4, "Marie Antoinette", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3956), null, "easy", "Czar Nicholas II,Elizabeth I,Henry VIII", false, null, "Which famous world leader is famed for the saying, &quot;Let them eat cake&quot;, yet is rumored that he/she never said it at all?", null },
                    { 114, 4, "Battle of Adrianople", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3878), null, "medium", "Battle of Thessalonica,Battle of Pollentia,Battle of Constantinople", false, null, "Which of the following battles is often considered as marking the beginning of the fall of the Western Roman Empire?", null },
                    { 96, 3, "Real Madrid C.F.", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3726), null, "easy", "FC Bayern Munich,Atletico Madrid,Manchester City F.C.", false, null, "Who won the UEFA Champions League in 2016?", null },
                    { 97, 3, "Boston Bruins", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3735), null, "medium", "Montreal Canadiens,New York Rangers,Toronto Maple Leafs", false, null, "Who won the 2011 Stanley Cup?", null },
                    { 98, 3, "The New York Giants &amp; The New England Patriots", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3742), null, "easy", "The Green Bay Packers &amp; The Pittsburgh Steelers,The Philadelphia Eagles &amp; The New England Patriots,The Seattle Seahawks &amp; The Denver Broncos", false, null, "Which two teams played in Super Bowl XLII?", null },
                    { 99, 3, "1991", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3751), null, "hard", "1990,2000,1987", false, null, "The Mazda 787B won the 24 Hours of Le Mans in what year?", null },
                    { 100, 3, "Russia", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3760), null, "medium", "Canada,United States,Germany", false, null, "What country hosted the 2014 Winter Olympics?", null },
                    { 101, 3, "Los Angeles Lakers", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3768), null, "medium", "Boston Celtics,Philadelphia 76ers,Golden State Warriors", false, null, "Which basketball team has attended the most NBA grand finals?", null },
                    { 102, 4, "Polio", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3776), null, "medium", "Cancer,Meningitis,HIV", false, null, "What disease crippled President Franklin D. Roosevelt and led him to help the nation find a cure?", null },
                    { 103, 4, "1789", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3785), null, "hard", "1823,1756,1799", false, null, "When did the French Revolution begin?", null },
                    { 94, 3, "2009", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3708), null, "easy", "2010,2007,2006", false, null, "Which year did Jenson Button won his first ever Formula One World Drivers&#039; Championship?", null },
                    { 104, 4, "Romantic", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3793), null, "hard", "Classic,Baroque,Renaissance", false, null, "Pianist Fr&eacute;d&eacute;ric Chopin was a composer of which musical era?", null },
                    { 106, 4, "John and Mary", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3810), null, "hard", "Joseph and Catherine,William and Elizabeth,George and Anne", false, null, "In the year 1900, what were the most popular first names given to boy and girl babies born in the United States?", null },
                    { 107, 4, "Richard III", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3817), null, "medium", "Edward V,Henry VII,James I", false, null, "Which king was killed at the Battle of Bosworth Field in 1485?", null },
                    { 108, 4, "6", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3826), null, "easy", "1,3,7", false, null, "How many manned moon landings have there been?", null },
                    { 109, 4, "Star Wars", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3834), null, "medium", "Jaws,Blade Runner,Alien", false, null, "America&#039;s Strategic Defense System during the Cold War was nicknamed after this famous movie.", null },
                    { 110, 4, "Aragon", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3844), null, "hard", "Galicia,Le&oacute;n,Navarre", false, null, "Spain was formed in 1469 with the marriage of Isabella I of Castile and Ferdinand II of what other Iberian kingdom?", null },
                    { 111, 4, "Catherine of Aragon", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3853), null, "medium", "Anne Boleyn,Jane Seymour,Catherine Parr", false, null, "Which of his six wives was Henry VIII married to the longest?", null },
                    { 112, 4, "Sumerian", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3860), null, "medium", "Maltese,Akkadian,Mandaic", false, null, "Which of the following is NOT classified as a Semetic language?", null },
                    { 113, 4, "Germany", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3868), null, "medium", "Britain,Belgium,France", false, null, "The Herero genocide was perpetrated in Africa by which of the following colonial nations?", null }
                });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CategoryId", "CorrectAnswer", "CreatedOn", "DeletedOn", "Difficulty", "IncorrectAnswers", "IsDeleted", "ModifiedOn", "Name", "Type" },
                values: new object[] { 105, 4, "Typhoons", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(3802), null, "medium", "Tornados,Economic depression,Samurai", false, null, "During the Mongolian invasions of Japan, what were the Mongol boats mostly stopped by?", null });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CategoryId", "CorrectAnswer", "CreatedOn", "DeletedOn", "Difficulty", "IncorrectAnswers", "IsDeleted", "ModifiedOn", "Name", "Type" },
                values: new object[] { 221, 6, "Michelangelo", new DateTime(2021, 2, 28, 16, 11, 17, 295, DateTimeKind.Utc).AddTicks(5033), null, "easy", "Leonardo da Vinci,Caravaggio,Rembrandt", false, null, "Who painted the biblical fresco The Creation of Adam?", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 77);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 78);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 79);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 80);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 81);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 82);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 83);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 84);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 85);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 86);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 87);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 88);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 89);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 90);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 91);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 92);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 93);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 94);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 95);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 96);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 97);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 98);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 99);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 100);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 101);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 102);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 103);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 104);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 105);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 106);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 107);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 108);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 109);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 110);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 111);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 112);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 113);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 114);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 115);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 116);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 117);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 118);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 119);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 120);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 121);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 122);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 123);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 124);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 125);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 126);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 127);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 128);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 129);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 130);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 131);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 132);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 133);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 134);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 135);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 136);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 137);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 138);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 139);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 140);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 141);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 142);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 143);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 144);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 145);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 146);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 147);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 148);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 149);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 150);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 151);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 152);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 153);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 154);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 155);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 156);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 157);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 158);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 159);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 160);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 161);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 162);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 163);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 164);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 165);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 166);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 167);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 168);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 169);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 170);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 171);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 172);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 173);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 174);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 175);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 176);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 177);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 178);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 179);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 180);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 181);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 182);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 183);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 184);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 185);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 186);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 187);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 188);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 189);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 190);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 191);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 192);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 193);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 194);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 195);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 196);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 197);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 198);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 199);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 200);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 201);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 202);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 203);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 204);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 205);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 206);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 207);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 208);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 209);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 210);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 211);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 212);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 213);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 214);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 215);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 216);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 217);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 218);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 219);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 220);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 221);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "0949a4df-fd9a-4b1e-b722-1f41c687c555");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "a966858d-ba9b-4778-8b61-a1cfd26da212");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "bcd779ac-a47a-45a1-9ac1-eb40fb5be7cf", new DateTime(2021, 2, 28, 13, 51, 40, 983, DateTimeKind.Utc).AddTicks(3893), "AQAAAAEAACcQAAAAED0wCeDa8C/5KqH3ssA881iYmOtEuNDwV6i/PPE7rdmf1UTwlrjF2e3pWxUJIP9Zsw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn" },
                values: new object[] { "e614a4ca-6dcb-4920-9ebd-9a70c340bcec", new DateTime(2021, 2, 28, 13, 51, 40, 994, DateTimeKind.Utc).AddTicks(8238) });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 979, DateTimeKind.Utc).AddTicks(1157));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 979, DateTimeKind.Utc).AddTicks(2097));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(2514));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5494));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5565));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5574));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5581));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5590));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5597));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5616));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5622));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5630));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5636));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 12,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5688));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 13,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5694));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 14,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5703));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 15,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5709));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 16,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5715));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 17,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5722));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 18,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5730));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 19,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5736));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 20,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5743));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 21,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5749));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 22,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5755));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 23,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5761));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 24,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5768));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 25,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5775));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 26,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5781));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 27,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5787));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 28,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5793));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 29,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5800));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 30,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5807));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 31,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5814));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 32,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5822));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 33,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5829));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 34,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5836));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 35,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5878));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 36,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5884));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 37,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5890));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 38,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5896));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 39,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5902));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 40,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5908));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 41,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5914));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 42,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5921));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 43,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5927));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 44,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5933));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 45,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5939));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 46,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5944));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 47,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5950));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 48,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5957));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 49,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5963));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 50,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5969));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 51,
                column: "CreatedOn",
                value: new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5975));
        }
    }
}
