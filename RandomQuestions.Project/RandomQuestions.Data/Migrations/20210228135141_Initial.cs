﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RandomQuestions.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    isBanned = table.Column<bool>(type: "bit", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    CorrectAnswer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Difficulty = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IncorrectAnswers = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Questions_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "0949a4df-fd9a-4b1e-b722-1f41c687c555", "Manager", "MANAGER" },
                    { 2, "a966858d-ba9b-4778-8b61-a1cfd26da212", "Member", "MEMBER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "DeletedOn", "Email", "EmailConfirmed", "IsDeleted", "LockoutEnabled", "LockoutEnd", "ModifiedOn", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "isBanned" },
                values: new object[,]
                {
                    { 1, 0, "bcd779ac-a47a-45a1-9ac1-eb40fb5be7cf", new DateTime(2021, 2, 28, 13, 51, 40, 983, DateTimeKind.Utc).AddTicks(3893), null, "manager@bo.com", false, false, true, null, null, "MANAGER@BO.COM", "MANAGER@BO.COM", "AQAAAAEAACcQAAAAED0wCeDa8C/5KqH3ssA881iYmOtEuNDwV6i/PPE7rdmf1UTwlrjF2e3pWxUJIP9Zsw==", null, false, "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN", false, "manager@bo.com", false },
                    { 2, 0, "e614a4ca-6dcb-4920-9ebd-9a70c340bcec", new DateTime(2021, 2, 28, 13, 51, 40, 994, DateTimeKind.Utc).AddTicks(8238), null, "JonathanRandall@Gmail.Com", false, false, false, null, null, null, null, null, null, false, null, false, "JonathanRandall@Gmail.Com", false }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 2, 28, 13, 51, 40, 979, DateTimeKind.Utc).AddTicks(1157), null, false, null, "category" },
                    { 2, new DateTime(2021, 2, 28, 13, 51, 40, 979, DateTimeKind.Utc).AddTicks(2097), null, false, null, "Animals" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CategoryId", "CorrectAnswer", "CreatedOn", "DeletedOn", "Difficulty", "IncorrectAnswers", "IsDeleted", "ModifiedOn", "Name", "Type" },
                values: new object[,]
                {
                    { 29, 2, "Tail", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5800), null, "hard", "Head,Teeth,Feet", false, null, "Unlike on most salamanders, this part of a newt is flat?", null },
                    { 30, 2, "Felidae", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5807), null, "medium", "Felinae,Felis,Cat", false, null, "What is the name of the family that the domestic cat is a member of?", null },
                    { 31, 2, "Pugs", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5814), null, "medium", "Bulldogs,Boxers,Chihuahua", false, null, "What dog bread is one of the oldest breeds of dog and has flourished since before 400 BCE.", null },
                    { 32, 2, "Hemocyanin", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5822), null, "hard", "Cytochrome,Iron,Methionine", false, null, "What is the name of the copper-rich protein that creates the blue blood in the Antarctic octopus?", null },
                    { 33, 2, "Frog", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5829), null, "hard", "Chicken,Horse,Fly", false, null, "Which species is a &quot;mountain chicken&quot;?", null },
                    { 34, 2, "Pufferfish", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5836), null, "medium", "Bass,Salmon,Mackerel", false, null, "The dish Fugu, is made from what family of fish?", null },
                    { 35, 2, "Murder", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5878), null, "easy", "Pack,Gaggle,Herd", false, null, "What is the collective noun for a group of crows?", null },
                    { 36, 2, "Gooty", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5884), null, "hard", "Hopper,Silver Stripe,Woebegone", false, null, "Which of the following is another name for the &quot;Poecilotheria Metallica Tarantula&quot;?", null },
                    { 37, 2, "Pan troglodytes", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5890), null, "medium", "Gorilla gorilla,Pan paniscus,Panthera leo", false, null, "What is the scientific name of the Common Chimpanzee?", null },
                    { 38, 2, "At the bottom of the ocean", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5896), null, "easy", "In the desert,On top of a mountain,Inside a tree", false, null, "By definition, where does an abyssopelagic animal live?", null },
                    { 28, 2, "Amphibian", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5793), null, "easy", "Fish,Reptiles,Mammals", false, null, "Which class of animals are newts members of?", null },
                    { 39, 2, "Portuguese Man-of-War", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5902), null, "hard", "Sea Wasp,Irukandji,Sea Nettle", false, null, "Which of these is a colony of polyps and not a jellyfish?", null },
                    { 41, 2, "Mad Cow disease", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5914), null, "medium", "Weil&#039;s disease,Milk fever,Foot-and-mouth disease", false, null, "What is the common term for bovine spongiform encephalopathy (BSE)?", null },
                    { 42, 2, "28", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5921), null, "easy", "30,26,24", false, null, "How many teeth does an adult rabbit have?", null },
                    { 43, 2, "Drone", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5927), null, "medium", "Soldier,Worker,Male", false, null, "What is the name for a male bee that comes from an unfertilized egg?", null },
                    { 44, 2, "Burrow", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5933), null, "easy", "Nest,Den,Dray", false, null, "What is the name of a rabbit&#039;s abode?", null },
                    { 45, 2, "Sloth", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5939), null, "medium", "Drove,Tribe,Husk", false, null, "What is the collective noun for bears?", null },
                    { 46, 2, "Mischief", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5944), null, "medium", "Pack,Race,Drift", false, null, "What is the collective noun for rats?", null },
                    { 47, 2, "Wake", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5950), null, "hard", "Ambush,Building,Gaze", false, null, "What is the collective noun for vultures?", null },
                    { 48, 2, "Hoatzin", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5957), null, "hard", "Cormorant,Cassowary,Secretary bird", false, null, "What bird is born with claws on its wing digits?", null },
                    { 49, 2, "Krill", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5963), null, "medium", "Lobsters,Shrimp,Crabs", false, null, "&quot;Decapods&quot; are an order of ten-footed crustaceans.  Which of these are NOT decapods?", null },
                    { 50, 2, "Chordata", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5969), null, "hard", "Echinodermata,Annelida,Placazoa", false, null, "To which biological phylum do all mammals, birds and reptiles belong?", null },
                    { 40, 2, "Brown", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5908), null, "easy", "Black,White,Yellow", false, null, "What colour is the female blackbird?", null },
                    { 27, 2, "Komodo dragon", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5787), null, "medium", "Japanese sea lion,Tasmanian tiger,Saudi gazelle", false, null, "Which of these species is not extinct?", null },
                    { 26, 2, "Ape", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5781), null, "hard", "Lion,Parrot,Wildcat", false, null, "What type of creature is a Bonobo?", null },
                    { 25, 2, "Tardar Sauce", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5775), null, "easy", "Sauce,Minnie,Broccoli", false, null, "What is Grumpy Cat&#039;s real name?", null },
                    { 2, 2, "Simien Jackel", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5494), null, "hard", "Ethiopian Coyote,Amharic Fox,Canis Simiensis", false, null, "What was the name of the Ethiopian Wolf before they knew it was related to wolves?", null },
                    { 3, 2, "River Horse (Greek)", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5565), null, "medium", "River Horse (Latin),Fat Pig (Greek),Fat Pig (Latin)", false, null, "What does &quot;hippopotamus&quot; mean and in what langauge?", null },
                    { 4, 2, "Toad", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5574), null, "medium", "Bird,Fish,Insect", false, null, "What type of animal is a natterjack?", null },
                    { 5, 2, "Homo Sapiens", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5581), null, "easy", "Homo Ergaster,Homo Erectus,Homo Neanderthalensis", false, null, "What is the scientific name for modern day humans?", null },
                    { 6, 2, "New Zealand", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5590), null, "easy", "South Africa,Australia,Madagascar", false, null, "The Kākāpō is a large, flightless, nocturnal parrot native to which country?", null },
                    { 7, 2, "Syrian Brown Bear", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5597), null, "hard", "California Grizzly Bear,Atlas Bear,Mexican Grizzly Bear", false, null, "Which species of Brown Bear is not extinct?", null },
                    { 8, 2, "Black", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5616), null, "medium", "White,Pink,Green", false, null, "What color/colour is a polar bear&#039;s skin?", null },
                    { 9, 2, "Seahorse", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5622), null, "easy", "Dolphin,Whale,Octopus", false, null, "Hippocampus is the Latin name for which marine creature?", null },
                    { 10, 2, "Goat", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5630), null, "medium", "Sheep,Camel,Llama", false, null, "Cashmere is the wool from which kind of animal?", null },
                    { 11, 2, "Ursus Maritimus", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5636), null, "medium", "Polar Bear,Ursus Spelaeus,Ursus Arctos", false, null, "What is the scientific name for the &quot;Polar Bear&quot;?", null },
                    { 12, 2, "Keratin", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5688), null, "medium", "Bone,Ivory,Skin", false, null, "What are rhino&#039;s horn made of?", null },
                    { 13, 2, "6", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5694), null, "easy", "2,4,0", false, null, "How many legs do butterflies have?", null },
                    { 14, 2, "Feliformia", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5703), null, "hard", "Haplorhini,Caniformia,Ciconiiformes", false, null, "What scientific suborder does the family Hyaenidae belong to?", null },
                    { 15, 2, "4", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5709), null, "hard", "8,2,6", false, null, "How many known living species of hyenas are there?", null },
                    { 16, 2, "Hyaenidae", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5715), null, "hard", "Canidae,Felidae,Eupleridae", false, null, "What scientific family does the Aardwolf belong to?", null }
                });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CategoryId", "CorrectAnswer", "CreatedOn", "DeletedOn", "Difficulty", "IncorrectAnswers", "IsDeleted", "ModifiedOn", "Name", "Type" },
                values: new object[,]
                {
                    { 17, 2, "Nervousness", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5722), null, "medium", "Excitement,Aggression,Exhaustion", false, null, "For what reason would a spotted hyena &quot;laugh&quot;?", null },
                    { 18, 2, "Pup", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5730), null, "easy", "Cub,Chick,Kid", false, null, "What do you call a baby bat?", null },
                    { 19, 2, "Acinonyx jubatus", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5736), null, "hard", "Panthera onca,Lynx rufus,Felis catus", false, null, "What is the scientific name of the cheetah?", null },
                    { 20, 2, "Melopsittacus undulatus", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5743), null, "hard", "Nymphicus hollandicus,Pyrrhura molinae,Ara macao", false, null, "What is the scientific name of the Budgerigar?", null },
                    { 21, 2, "Foxes", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5749), null, "medium", "Pigeons,Bears,Alligators", false, null, "Which animal was part of an Russian domestication experiment in 1959?", null },
                    { 22, 2, "Tasmania, Australia", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5755), null, "medium", "Baluchistan, Pakistan,Wallachia, Romania,Oregon, United States", false, null, "The now extinct species &quot;Thylacine&quot; was native to where?", null },
                    { 23, 2, "Canis Lupus", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5761), null, "hard", "Canis Aureus,Canis Latrans,Canis Lupus Lycaon", false, null, "What is the Gray Wolf&#039;s scientific name?", null },
                    { 24, 2, "Tuatara", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5768), null, "hard", "Komodo Dragon,Gila Monster,Green Iguana", false, null, "Which of these animals is NOT a lizard?", null },
                    { 51, 2, "King Cobra", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(5975), null, "medium", "Green Anaconda,Inland Taipan,Yellow Bellied Sea Snake", false, null, "What is the world&#039;s longest venomous snake?", null },
                    { 1, 1, "correct_answer", new DateTime(2021, 2, 28, 13, 51, 40, 981, DateTimeKind.Utc).AddTicks(2514), null, "difficulty", "incorrect_answers/0,incorrect_answers/1,incorrect_answers/2", false, null, "question", null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_CategoryId",
                table: "Questions",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
