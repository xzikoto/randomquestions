﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RandomQuestions.Web.Models.Scores
{
    public class CurrentScoreViewModel
    {
        List<string> Scores = new List<string>();

        public int rightAnswers { get; set; }
        public int allTimeRightAnswers { get; set; }

        public int wrongAnswers { get; set; }
        public int allTimewrongAnswers { get; set; }

        

    }
}
