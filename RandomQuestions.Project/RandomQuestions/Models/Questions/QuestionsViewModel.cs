﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RandomQuestions.Web.Models.Questions
{
    public class QuestionsViewModel
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string CorrectAnswer { get; set; }
        public List<string> IncorrectAnswers { get; set; }
    }
}
