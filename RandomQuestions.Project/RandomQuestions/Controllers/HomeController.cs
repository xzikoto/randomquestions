﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RandomQuestions.Models;
using RandomQuestions.Services.Contracts;
using RandomQuestions.Web.Models.Questions;
using RandomQuestions.Web.Models.Scores;
using Microsoft.AspNetCore.Authorization;

namespace RandomQuestions.Controllers
{
    [Authorize(Roles = "Member,Manager")]
    public class HomeController : Controller
    {
        private readonly IMapper mapper;
        private readonly ILogger<HomeController> _logger;
        private readonly IQuestionService questionService;

        public HomeController(IMapper mapper,ILogger<HomeController> logger, IQuestionService questionService)
        {
            _logger = logger;
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.questionService = questionService ?? throw new ArgumentNullException(nameof(questionService));
        }

        public IActionResult Index()
        {
            var number = new Random(DateTime.Now.Millisecond);

            var questionsDTO = questionService.GetFiveRandomQuestions(number);

            var questionsViewModel = mapper.Map<ICollection<QuestionsViewModel>>(questionsDTO);
            return View(questionsViewModel);
        }

        public async Task<IActionResult> CurrentScoreWithChart()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var userLastScore = questionService.GetLastAnswer(Convert.ToInt32(userId));
            var allTimeScore = questionService.GetAllTimeScore(Convert.ToInt32(userId));

            int rightAnswers = 0;

            foreach (var item in userLastScore.Scores)
            {
                if (item.Contains("TRUE"))
                {
                    rightAnswers++;
                }
            }
            
            var answerViewModel = mapper.Map<CurrentScoreViewModel>(userLastScore);
            answerViewModel.rightAnswers = rightAnswers;
            answerViewModel.wrongAnswers = 5 - rightAnswers;

            answerViewModel.allTimeRightAnswers = allTimeScore.First();
            answerViewModel.allTimewrongAnswers = allTimeScore.Skip(1).First();

            return View("CurrentScoreWithChart", answerViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CheckAnswers(string[] data)
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var answersToCheck = await questionService.CheckAnswers(data, Convert.ToInt32(userId));
            
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> CheckSingleAnswer(string data)
        {
            //TO DO:MUST FIX THE MAPPING FROM RADIO BUTTONS!
           // var answersToCheck = await questionService.CheckSingleAnswers(data);
            return Ok();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
