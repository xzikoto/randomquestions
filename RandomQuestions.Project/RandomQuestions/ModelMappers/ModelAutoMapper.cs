﻿using AutoMapper;
using RandomQuestions.Services.DTOs;
using RandomQuestions.Web.Models.Questions;
using RandomQuestions.Web.Models.Scores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RandomQuestions.Web.ModelMappers
{
    public class ModelAutoMapper : Profile
    {
        public ModelAutoMapper()
        {
            CreateMap<QuestionDTO, QuestionsViewModel>().ReverseMap();

            CreateMap<AnswerDTO, CurrentScoreViewModel>().ReverseMap();
        }
    }
}
