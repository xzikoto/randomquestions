<img src="images/logo.jpg" height="300px" width="300px">

## **Project Description**
Welcome to <b>RandomQuestions</b>, a web application that expands your knowledge!<br />

Quiz web application in which people can answer FIVE random questions from
FIVE different categories (for example sports, art, history, technology, animals) and get results
for the number of right and wrong answers. 
<br />



<h1>HOW TO RUN THE APPLICATION:</h1>
<h3>Instrunctions</h3> 
1. Clone the repository to your computer <br>
2. Double-click the RandomQuestions.sln file <br>
3. Check the connection string in appsettings.json -  there we set our local connection with the Database <br>
4. Enter F5 button or click the green play button in order to power up our Web Application <br>
